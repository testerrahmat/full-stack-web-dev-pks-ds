<?php

trait Hewan 
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        echo $this->nama . " sedang " . $this->keahlian;
        echo "<br>";
    }
}

trait Fight 
{
    public $attackPower;
    public $defencePower;
    

    public function serang($namapenyerang, $namadiserang, $darahdiserang, $attackPowerPenyerang){
        echo $this->nama = $namapenyerang . " sedang menyerang " . $this->nama = $namadiserang; 
        echo "<br>";
        $this->diserang($namadiserang, $darahdiserang, $attackPowerPenyerang);
    }

    public function diserang($namadiserang, $darahdiserang, $attackPowerPenyerang){
        echo $this->nama = $namadiserang  . " sedang diserang ";
        echo "<br>";
        echo "Darah " . $this->nama = $namadiserang . " sekarang : " . $this->darah = $darahdiserang - $this->attackPower = $attackPowerPenyerang;
        echo "<br>";
    }
}

class Elang{
    use Hewan, Fight;

    public function getInfoHewan(){
        echo "---Detail Hewan---";
        echo "<br>";
        echo "Nama Hewan : " . $this->nama;
        echo "<br>";
        echo "Darah : " . $this->darah;
        echo "<br>";
        echo "Jumlah Kaki : " . $this->jumlahKaki;
        echo "<br>";
        echo "Kelahlian : " . $this->keahlian;
        echo "<br>";
        echo "AttackPower : " . $this->attackPower;
        echo "<br>";
        echo "DefencePower : " . $this->defencePower;
        echo "<br>";
        
    }
}

class Harimau{
    use Hewan, Fight;

    public function getInfoHewan(){
        echo "---Detail Hewan---";
        echo "<br>";
        echo "Nama Hewan : " . $this->nama;
        echo "<br>";
        echo "Darah : " . $this->darah;
        echo "<br>";
        echo "Jumlah Kaki : " . $this->jumlahKaki;
        echo "<br>";
        echo "Kelahlian : " . $this->keahlian;
        echo "<br>";
        echo "AttackPower : " . $this->attackPower;
        echo "<br>";
        echo "DefencePower : " . $this->defencePower;
        echo "<br>";
    }
}

$elang = new Elang;
$elang->nama = "Elang";
$elang->jumlahKaki = 2;
$elang->keahlian = 'terbang tinggi';
$elang->attackPower = 10;
$elang->defencePower = 5;
$elang->getInfoHewan();
echo "<br>";

$harimau = new Harimau;
$harimau->nama = "Harimau";
$harimau->jumlahKaki = 4;
$harimau->keahlian = 'lari cepat';
$harimau->attackPower = 7;
$harimau->defencePower = 8;
$harimau->getInfoHewan();
echo "<br>";

$harimau->atraksi();
$elang->atraksi();
echo "<br>";
$elang->serang($elang->nama, $harimau->nama, $harimau->darah , $elang->attackPower);
echo "<br>";

?>
<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegisterEvent;
use App\Http\Controllers\Controller;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($allRequest);

        do {
            $otp = mt_rand( 100000 , 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while ($check);
        
        $validUntil = Carbon::now()->addMinutes(15);
        
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        event(new RegisterEvent($user));

        return response()->json([
            'success' => true,
            'message' => 'Data User berhasil dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
            ]);
    }
}
<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Mail;
use App\Events\RegenerateEvent;
use App\Mail\RegenerateAuthorMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailToRegenerateAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateEvent  $event
     * @return void
     */
    public function handle(RegenerateEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenerateAuthorMail($event->otp_code));
    }
}